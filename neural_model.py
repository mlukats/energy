import torch
from torch import nn
from torch.autograd import Variable


class LinearRegressionModel(nn.Module):

    def __init__(self, input_dim, output_dim):
        torch.manual_seed(1)
        super(LinearRegressionModel, self).__init__()
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.w = torch.nn.Parameter(torch.nn.init.xavier_normal_(torch.empty(input_dim, output_dim).uniform_(0, 1)))
        self.b = torch.nn.Parameter((torch.empty(output_dim).uniform_(0, 1)))

    def activation(self):
        return torch.nn.ReLU()

    def batcNormalize(self, out):
        outSize = list(out.shape)
        arguments = list(filter(lambda x: x != 1, outSize))
        batched = self.batch_norm(out.view(*arguments))
        return batched.view(*outSize)

    def forward(self, x):
        x = x.view((x.shape[0], 1, self.input_dim))

        exp_w = self.w.expand(x.shape[0], self.w.size(0), self.w.size(1))
        out = torch.bmm(x, exp_w) + self.b
        out = self.activation()(out)

        return out.view(x.shape[0])

    def fit(self, x_train, y_correct, epochs=120, batch_size=60):
        criterion = nn.MSELoss()
        l_rate = 0.0001
        last_loss = 0
        optimiser = torch.optim.Adam(self.parameters(), lr=l_rate)  # Stochastic Gradient Descent
        inputs = torch.from_numpy(x_train).type(torch.FloatTensor)
        labels = torch.from_numpy(y_correct).type(torch.FloatTensor)
        for epoch in range(epochs):
            epoch += 1
            start = 0
            for i in range(0, labels.shape[0] + 1, batch_size):
                if i != 0:
                    batch_inputs = Variable(inputs[start:i])
                    batch_labels = Variable(labels[start:i])

                    optimiser.zero_grad()
                    outputs = self.forward(batch_inputs)
                    outputs = outputs.reshape(outputs.shape[0])
                    loss = torch.sqrt(criterion(outputs, batch_labels))
                    loss.backward()  # back props
                    last_loss = loss.data.item()
                    optimiser.step()  # update the parameters

                start = i
                if epoch % 10 == 0 and i == batch_size:
                    print('epoch {}, loss {}'.format(epoch, last_loss))
        print('activation: {}, Epochs: {},  batch size: {}, final loss: {}, l_rate: {}, optimiser: {}'
              .format(str(self.activation()), epochs, batch_size, last_loss, l_rate, str(optimiser)))

    def predict(self, y_predict):
        return self.forward(Variable(torch.from_numpy(y_predict)).type(torch.FloatTensor)).data.numpy()

import matplotlib.pyplot as plt
import numpy as np
from sklearn.ensemble import RandomForestRegressor
from utility import printPredictionError
from datapreperation import temperatureData, trainingTargets, holidayData, mainData, validationFeatures, \
    validationTargets, trainingFeaturesTargets, demandMinMax, validationRawDataFrame, trainingRawDataFrame
from neural_model import LinearRegressionModel


def getClassifier():
    return RandomForestRegressor(random_state=1)


def neuralnetworkModel():
    X_train, Y_train = trainingFeaturesTargets(False)
    X_validation = validationFeatures()

    classifier = LinearRegressionModel(X_train.shape[1], 1)
    classifier.fit(X_train, Y_train)
    predictions = classifier.predict(X_validation)
    predictions = np.multiply(predictions, demandMinMax()['max'])
    header = "Neural Network model"
    plt.plot(predictions, linestyle='--', color='pink', label=header)
    printPredictionError(predictions, header)
    return predictions


def holidayAndDemand():
    df_de = mainData()
    df_temp = temperatureData()
    df_de = df_de.merge(df_temp, how='left', on=['year', 'month', 'day', 'hour'])
    df_holiday = holidayData()
    df_de = df_de.merge(df_holiday, how='left', on=['month', 'day'])
    df_de = df_de.fillna(0)
    # Lisan tarbimise t-1
    df_de.loc[:, 'demand1'] = df_de['Demand MW']
    df_de['demand1'] = df_de.demand1.shift(-1)
    df_de = df_de[:-1]
    # Lisan tarbimise t-2
    df_de.loc[:, 'demand2'] = df_de.demand1.shift(-1)
    df_de = df_de[:-1]
    df_de_predict = df_de[(df_de.month == 8) & (df_de.year == 2015)]
    df_de_train = df_de[(df_de.month != 8)]
    Y_train = df_de_train['Demand MW'].fillna(0).values
    array = df_de_train.drop(['country', 'Demand MW'], axis=1).values
    X_train = array
    array = df_de_predict.drop(['country', 'Demand MW'], axis=1).values
    X_validation = array
    CART = getClassifier()
    CART.fit(X_train, Y_train)
    predictions = CART.predict(X_validation)

    header = "holidayAndDemand"
    printPredictionError(predictions, header)
    plt.plot(predictions, color='black', linestyle='--', label=header)

    return predictions


def predictTemperature():
    df_temp = temperatureData()
    df_de_predict = validationRawDataFrame()
    df_de_predict = df_de_predict.merge(df_temp, how='left', on=['year', 'month', 'day', 'hour'])
    df_de_train = trainingRawDataFrame()
    df_de_train = df_de_train.merge(df_temp, how='left', on=['year', 'month', 'day', 'hour'])

    Y_train = trainingTargets()

    array = df_de_train.drop(['Demand MW'], axis=1).values
    X_train = array
    array = df_de_predict.drop(['Demand MW'], axis=1).values
    X_validation = array
    CART = getClassifier()
    CART.fit(X_train, Y_train)
    predictions = CART.predict(X_validation)

    header = "predictTemperature"
    printPredictionError(predictions, header)
    plt.plot(predictions, color='green', label=header)

    return predictions


def predictOnDayData():
    df_de_predict = validationRawDataFrame()
    df_de_train = trainingRawDataFrame()
    Y_train = trainingTargets()
    array = df_de_train.drop(['Demand MW', 'day'], axis=1).values
    X_train = array
    array = df_de_predict.drop(['Demand MW', 'day'], axis=1).values
    X_validation = array

    CART = getClassifier()
    CART.fit(X_train, Y_train)
    predictions = CART.predict(X_validation)

    header = "predictOnDayData"
    printPredictionError(predictions, header)
    plt.plot(predictions, color='red', label=header)

    return predictions

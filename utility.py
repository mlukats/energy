from datapreperation import validationTargets
import numpy as np


def printPredictionError(prediction, header, rawData=True):
    error = (prediction - validationTargets(rawData))
    rms = np.sqrt(np.mean(np.square(error)))
    print("RMSE: " + str(header), rms)

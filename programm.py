import pandas as pd
import matplotlib.pyplot as plt
from datapreperation import validationTargets
from models import predictTemperature, holidayAndDemand, predictOnDayData, neuralnetworkModel

pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 100000)


def runModel():
    neuralnetworkModel()
    # predictTemperature()
    # holidayAndDemand()
    # predictOnDayData()

    plt.plot(validationTargets(), color='blue', label="y_validation", alpha=0.5)
    plt.figure(1, figsize=(12, 6))
    plt.legend(bbox_to_anchor=(0.05, 1), loc=2, borderaxespad=0.)
    plt.show()


if __name__ == '__main__':
    print("Main Started")
    runModel()
    print("Main Ended")

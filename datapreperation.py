import pandas as pd
import numpy as np
from sklearn import preprocessing

weekdayColumnHeaders = ['s', 'm', 't', 'w', 't', 'f', 's']
monthColumnHeaders = ['J', 'F', 'M', 'A', 'M', 'J', 'J', 'A', 'S', 'O', 'N', 'D']
yearColumnHeaders = [2013, 2014, 2015]
monthColumnHeadersEnumerated = list(range(1, 13))
yearColumnHeadersEnumerated = list(range(1, 4))
dropColumns = ['Demand MW', 'year', 'day', 'weekday', 'hour', 'month']


def mainData():
    df_de = pd.read_csv('input/Electricity_Demand_ENTSO_E_UTC.csv', sep=',')
    # df_temp.Temperature=df_temp['Temperature'].rolling(window=24).mean()
    df_de = df_de[(df_de.country == 'EE') & (df_de.year > 2012)]
    df_de = df_de.drop(['country'], axis=1)
    df_de.loc[:, 'weekday'] = pd.to_datetime(df_de.year * 10000 + df_de.month * 100 + df_de.day,
                                             format='%Y%m%d').dt.dayofweek
    return df_de


def demandMinMax():
    main_data = mainData()
    return {'min': main_data['Demand MW'].min(),
            'max': main_data['Demand MW'].max()}




def validationFeatures():
    df_de_predict = validationRawDataFrame()
    df_de_predict_weekdays = pd.get_dummies(df_de_predict['weekday'])
    df_de_predict_weekdays.columns = weekdayColumnHeaders
    df_de_predict_hour = pd.get_dummies(df_de_predict['hour'])
    df_de_predict_month = pd.DataFrame(columns=monthColumnHeadersEnumerated)
    df_de_predict_month = pd.concat([df_de_predict_month, pd.get_dummies(df_de_predict['month'])]).fillna(0)
    df_de_predict_month.columns = monthColumnHeaders
    df_de_predict_year = pd.DataFrame(columns=yearColumnHeaders)
    df_de_predict_year = df_de_predict_year.append(pd.get_dummies(df_de_predict['year'])).fillna(0)
    df_de_predict = pd.concat(
        [df_de_predict_year, df_de_predict, df_de_predict_weekdays, df_de_predict_hour, df_de_predict_month],
        axis=1,
        join='inner')
    df_de_predict = df_de_predict.drop(dropColumns, axis=1)
    df_de_predict = df_de_predict.drop(yearColumnHeaders, axis=1)

    array = df_de_predict.values
    X_validation = array
    print("X_validation\n", df_de_predict[:2])
    return X_validation

def validationRawDataFrame():
    df_de = mainData()
    return df_de.query('month==8 and year==2015')

def validationTargets(raw=True):
    df_de = validationRawDataFrame()
    x = df_de['Demand MW'].fillna(0).values
    if raw:
        return x
    else:
        return demandToMinMax(x)




def trainingFeatures():
    df_de_train = trainingRawDataFrame()
    df_de_train_weekdays = pd.get_dummies(df_de_train['weekday'])
    df_de_train_weekdays.columns = weekdayColumnHeaders
    df_de_train_hour = pd.get_dummies(df_de_train['hour'])
    df_de_train_month = pd.DataFrame(columns=monthColumnHeadersEnumerated)
    df_de_train_month = pd.concat([df_de_train_month, pd.get_dummies(df_de_train['month'])]).fillna(0)
    df_de_train_month.columns = monthColumnHeaders
    df_de_train_year = pd.DataFrame(columns=yearColumnHeaders)
    df_de_train_year = df_de_train_year.append(pd.get_dummies(df_de_train['year'], columns=yearColumnHeaders)).fillna(0)
    df_de_train = pd.concat([df_de_train_year, df_de_train, df_de_train_weekdays, df_de_train_hour, df_de_train_month],
                            axis=1,
                            join='inner')
    df_de_train = df_de_train.drop(dropColumns, axis=1)
    df_de_train = df_de_train.drop(yearColumnHeaders,
                                   axis=1)  # droping yearly columns to improve neuralnetwork prediction
    array = df_de_train.values
    X_train = array
    print("X_train\n", df_de_train[:2])
    return X_train

def trainingRawDataFrame():
    df_de = mainData()
    return df_de.drop(validationRawDataFrame().index)

def trainingTargets(raw=True):
    df_de = trainingRawDataFrame()
    x = df_de['Demand MW'].fillna(0).values
    if raw:
        return x
    else:
        return demandToMinMax(x)


def demandToMinMax(x):
    min_max = demandMinMax()
    x = np.append(x, min_max['max'])
    x = np.append(x, min_max['min'])
    min_max_scaler = preprocessing.MinMaxScaler()
    x_scaled = pd.DataFrame(min_max_scaler.fit_transform(x.reshape(-1, 1))).values.reshape(1, -1)[0]
    returnval = x_scaled[:-2]
    return returnval


def trainingFeaturesTargets(raw=True):
    return trainingFeatures(), trainingTargets(raw)


def temperatureData():
    df_temp = pd.read_csv('input/EE_temps.csv', sep=',')
    df_temp = df_temp.drop(['Region'], axis=1)
    return df_temp


def holidayData():
    df_holiday = pd.read_csv('input/holiday.csv', sep=',')
    return df_holiday
